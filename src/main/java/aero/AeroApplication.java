package aero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class AeroApplication {
	
	//@RequestMapping({"/welcome", "/"})
	public static void main(String[] args) {
		SpringApplication.run(AeroApplication.class, args);
	}

}
