package aero;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


import controller.Login;
import models.RegisterForm;
import models.User;
import repository.UserRepository;

@Controller
public class WebController implements WebMvcConfigurer {
	
	//@Autowired
	
	private final UserRepository usersRepo;
	
	public WebController(UserRepository usersRepo) {
		this.usersRepo = usersRepo;
	}

	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/results").setViewName("results");
	}
	
	@GetMapping("/")
	public String showForm() {
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET) 
	public String displayLogin(Model model) { 
	    model.addAttribute("login", new Login()); 
	    return "login"; 
	}
	
    @GetMapping("/register")
    public String initRegisterForm(Model model){
    	model.addAttribute("user", new User());
    	System.out.println("get mapping register");
        return "register";
    }	

    
   
    @RequestMapping(value="/register", method = RequestMethod.POST)
	public String processRegisterForm(@Valid @ModelAttribute("user") User user,BindingResult bindingResult) {
	//	User userForm = new User();
		System.out.print("user form procces");
	//	model.put("userForm", userForm);
		System.out.print(user);
		this.usersRepo.save(user);
		return "redirect:/login";
		
	}

    
    /*        
	@PostMapping("/register")
	public String checkPersonInfo(@Valid RegisterForm registerForm, BindingResult bindingResult) {
		System.out.print("user form procces");
		if (bindingResult.hasErrors()) {
			System.out.print(registerForm);
			System.out.print("=========");
			return "redirect:/register";
			
		}

		
		return "redirect:/results";
	}
*/	

}
