package service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.activation.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import models.Flight;
import repository.FlightRepository;

public class FlightService implements FlightServiceInterface {
	
	@Autowired
	DataSource dataSource;
	
	@Autowired
	FlightRepository flightRepository;
	
	public FlightService() {
		
	}
	
	public Flight getFlight(Long id) {
		return flightRepository.findById(id);
		
	}
	
	public List<Flight> getAll() {
		return (List<Flight>) flightRepository.findAll();
	}
/*
	@Override
	public Flight findOne(Integer id) {
		// TODO Auto-generated method stub
		return flightRepository.fin
	}
*/
	@Override
	public List<Flight> findAll() {
		// TODO Auto-generated method stub
		return (List<Flight>) flightRepository.findAll();
	}

	@Override
	public Page<Flight> findAll(Pageable page) {
		// TODO Auto-generated method stub
		return (Page<Flight>) flightRepository.findAll();
	}

	@Override
	public Flight save(Flight flight) {
		// TODO Auto-generated method stub
		return flightRepository.save(flight);
	}

	@Override
	public void remove(Integer id) {
		// TODO Auto-generated method stub
		flightRepository.remove(id);
		
	}

	@Override
	public Flight save(Integer id, Integer number, Date date_a, Date date_b, String aeroport_a, String aeroport_b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Flight findOne(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public Flight findByUsername(String username) {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
