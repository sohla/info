package service;

import java.util.List;

import javax.activation.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import models.Aeroport;
import repository.AeroportRepository;

public class AeroportService implements AeroportServiceInterface {
	
	@Autowired
	DataSource dataSource;
	
	@Autowired
	AeroportRepository aeroportRepository;
	
	public Aeroport getAeroport(Long id) {
		return aeroportRepository.findById(id);
		
	}
	
	public List<Aeroport> getAll() {
		return (List<Aeroport>) aeroportRepository.findAll();
	}

	@Override
	public Aeroport findOne(Long id) {
		// TODO Auto-generated method stub
		return aeroportRepository.findById(id);
	}

	@Override
	public List<Aeroport> findAll() {
		// TODO Auto-generated method stub
		return (List<Aeroport>) aeroportRepository.findAll();
	}

	@Override
	public Aeroport save(Aeroport aeroport) {
		// TODO Auto-generated method stub
		return aeroportRepository.save(aeroport);
	}

	@Override
	public void remove(Integer id) {
		// TODO Auto-generated method stub
		aeroportRepository.remove(id);
	}



	@Override
	public Aeroport findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Aeroport> findAll(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Aeroport save(Integer id, String name) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
