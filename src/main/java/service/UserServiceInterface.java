package service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import models.User;



public interface UserServiceInterface {
	
	//User findOne (Long id);
	
	List<User> findAll();
	
	Page<User> findAll(Pageable page);
	
	User save (User user);
	
	void remove (Long id);
	
	User save (String username, String password,String first_name, String last_name);
	
	User findByUsername(String username);

	void remove(Integer id);

	Optional<User> findOne(Integer id);

	User saveUser(User user);

}
