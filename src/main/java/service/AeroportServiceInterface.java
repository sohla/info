package service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import models.Aeroport;

public interface AeroportServiceInterface {

	Aeroport findOne (Long id);
	
	List<Aeroport> findAll();
	
	Page<Aeroport> findAll(Pageable page);
	
	Aeroport save (Aeroport user);
	
	void remove (Integer id);
	
	Aeroport save (Integer id, String name);
	
	Aeroport findByName(String name);
	
}
