package service;

import java.util.List;

import javax.activation.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import models.Aeroport;
import models.Ticket;
import repository.TicketRepository;

public class TicketService implements TicketServiceInterface {
	
	@Autowired
	DataSource dataSource;
	
	@Autowired
	TicketRepository ticketRepository;
	
	
	public TicketService() {
		
	}
	
	public Ticket getTicket(Integer id) {
		return ticketRepository.findById(id);
		
	}
	
	public List<Ticket> getAll() {
		return (List<Ticket>) ticketRepository.findAll();
	}

	@Override
	public Ticket findOne(Integer id) {
		// TODO Auto-generated method stub
		return ticketRepository.findById(id);
	}

	@Override
	public List<Ticket> findAll() {
		// TODO Auto-generated method stub
		return ticketRepository.findAll();
	}

	@Override
	public Page<Ticket> findAll(Pageable page) {
		// TODO Auto-generated method stub
		return (Page<Ticket>) ticketRepository.findAll();
	}

	@Override
	public Ticket save(Ticket ticket) {
		// TODO Auto-generated method stub
		return ticketRepository.save(ticket);
	}

	@Override
	public void remove(Integer id) {
		// TODO Auto-generated method stub
		ticketRepository.remove(id);
		
	}
/*
	@Override
	public Ticket save(String start,String finish, String seat_a, String seat_b, String date_reservation,
			String date_sold, String user, String first_name, String last_name) {
		// TODO Auto-generated method stub
		return ticketRepository.save(start,finish, seat_a, seat_b, date_reservation,
				date_sold, user, first_name, last_name);
	}
*/
	@Override
	public Ticket findByUId(Integer id) {
		// TODO Auto-generated method stub
		return ticketRepository.findById(id);
	}

	@Override
	public Ticket save(Aeroport user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Ticket save(Integer id, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ticket save(String start, String finish, String seat_a, String seat_b, String date_reservation,
			String date_sold, String user, String first_name, String last_name) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
