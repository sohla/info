package service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import models.Flight;


public interface FlightServiceInterface {
	
	Flight findOne (Integer id);
	
	List<Flight> findAll();
	
	Page<Flight> findAll(Pageable page);
	
	Flight save (Flight user);
	
	void remove (Long id);
	
	Flight save (Integer id,Integer number, Date date_a, Date date_b, String aeroport_a, String aeroport_b);

	void remove(Integer id);
	
//	Flight findByUsername(String username);

}
