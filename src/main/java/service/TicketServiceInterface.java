package service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import models.Aeroport;
import models.Ticket;

public interface TicketServiceInterface {

	//Ticket findOne (Long id);
	
	List<Ticket> findAll();
	
	Page<Ticket> findAll(Pageable page);
	
	Ticket save (Aeroport user);
	
	void remove (Long id);
	
	Ticket save (Integer id, String name);
	
	Ticket findByUId(Integer id);

	Ticket findOne(Integer id);

	Ticket save(Ticket ticket);

	void remove(Integer id);

	Ticket save(String start, String finish, String seat_a, String seat_b, String date_reservation,
			String date_sold, String user, String first_name, String last_name);
	
}
