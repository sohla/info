package service;

import java.util.List;
import java.util.Optional;

import javax.activation.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import models.User;
import repository.UserRepository;

@Service
public class UserService implements UserServiceInterface {
		
	@Autowired
	DataSource dataSource;
	
	@Autowired
	UserRepository userRepository;


	
	//@Override
	public Optional<User> getUser(Integer id) {
		return userRepository.findById(id);
		
	}
	
	//@Override
	public List<User> getAll() {
		return (List<User>) userRepository.findAll();
	}
	
	//@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public Optional<User> findOne(Integer id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id);
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return (List<User>) userRepository.findAll();
	}

	@Override
	public Page<User> findAll(Pageable page) {
		// TODO Auto-generated method stub
		return (Page<User>) userRepository.findAll();
	}

	@Override
	public void remove(Integer id) {
		// TODO Auto-generated method stub
		userRepository.remove(id);
		
	}

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return userRepository.findByUsername(username);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User save(String username, String password, String first_name, String last_name) {
		// TODO Auto-generated method stub
		return null;
	}
}
