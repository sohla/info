package controller;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import models.RegisterForm;
import models.User;
import repository.UserRepository;

@Controller
public class UserController  {
	
	private static final String VIEWS_USER_CREATE_OR_UPDATE_FORM = "users/createOrUpdateOwnerForm";
	private final UserRepository users;

	public UserController(UserRepository users) {
		this.users = users;
	}
	
//	@RequestMapping("/login")
//	public String login() {
 //   	return "login";
 //   }	
	
//    @RequestMapping(value = "register")
//    public String addUser(Model model){
//    	model.addAttribute("registerform", new RegisterForm());
//        return "register";
//    }

	
	
	
	@RequestMapping(value = "saveuser", method = RequestMethod.GET)
    public String saveSUUS() {
		System.out.println("register form");
		return null;
   	
    } 
	
		 
	
	@RequestMapping(value = "/saveuser", method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute("signupform") RegisterForm registerForm, BindingResult bindingResult) {
		System.out.println("register form");
		System.out.println(bindingResult.toString());
    	if (!bindingResult.hasErrors()) { // validation errors
    		if (registerForm.getPassword().equals(registerForm.getPasswordRepeat())) { // check password match		
	    		String pwd = registerForm.getPassword();
	
		    	User newUser = new User();

		    	newUser.setPassword(pwd);
		    	newUser.setUsername(registerForm.getUsername());
		    	newUser.setFirst_name(registerForm.getFirstName());
		    	newUser.setFirst_name(registerForm.getLastName());
		    	newUser.setFirst_name(registerForm.getDate());
		    //	newUser.setFirst_name(registerForm.getRole());
		    	newUser.setRole("USER");
		    	if (users.findByUsername(registerForm.getUsername()) == null) {
		    		users.save(newUser);
		    	}
		    	else {
	    			bindingResult.rejectValue("username", "error.userexists", "Username already exists");    	
	    			return "signup";		    		
		    	}
    		}
    		else {
    			bindingResult.rejectValue("passwordCheck", "error.pwdmatch", "Passwords does not match");    	
    			return "signup";
    		}
    	}
    	else {
    		return "signup";
    	}
    	return "redirect:/login";    	
    }      
    
	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}
	@GetMapping("/users/new")
	public String initCreationForm(Map<String, Object> model) {
		
		User user = new User();	
		model.put("user", user);
		return VIEWS_USER_CREATE_OR_UPDATE_FORM;
		
	}
	
	@PostMapping("/users/new")
	public String processCreationForm(@Valid User user, BindingResult result) {
		if (result.hasErrors()) {
			return VIEWS_USER_CREATE_OR_UPDATE_FORM;
		}
		else {
			this.users.save(user);
			return "redirect:/owners/" + user.getId();
		}
	}
	
	@GetMapping("/users/find")
	public String initFindForm(Map<String, Object> model) {
		model.put("user", new User());
		return "users/findUsers";
	}
	
	@GetMapping("/users")
	public String processFindForm(User user,BindingResult result, Map<String, Object> model) {
		System.out.print("users");
		if (user.getLast_name() == null) {
			user.setLast_name(""); // empty string signifies broadest possible search
		}

		// find owners by last name
		Collection<User> results = this.users.findByLastName(user.getLast_name());
		if (results.isEmpty()) {
			// no owners found
			result.rejectValue("lastName", "notFound", "not found");
			return "owners/findOwners";
		}
		else if (results.size() == 1) {
			// 1 owner found
			user = results.iterator().next();
			return "redirect:/owners/" + user.getId();
		}
		else {
			// multiple owners found
			model.put("selections", results);
			return "owners/ownersList";
		}
		
	}
	
	@GetMapping("/users/{id}/edit")
	public String initUpdateOwnerForm(@PathVariable("id") int id, Model model) {
		Optional<User> user = this.users.findById(id);
		model.addAttribute(user);
		return VIEWS_USER_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/owners/{ownerId}/edit")
	public String processUpdateOwnerForm(@Valid User user, BindingResult result,
			@PathVariable("ownerId") int id) {
		if (result.hasErrors()) {
			return VIEWS_USER_CREATE_OR_UPDATE_FORM;
		}
		else {
			user.setId(id);
			this.users.save(user);
			return "redirect:/users/{id}";
		}
	}
	@GetMapping("/users/{id}")
	public ModelAndView showOwner(@PathVariable("id") int id) {
		ModelAndView mav = new ModelAndView("users/userDetails");
		Optional<User> user = this.users.findById(id);
//  za karte kupljene / rezervisane		
//		for (Pet pet : user.getPets()) {
//			pet.setVisitsInternal(visits.findByPetId(pet.getId()));
//		}
	
		mav.addObject(user);
		return mav;
	}
	
	
	
	
}
