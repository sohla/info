package controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import models.User;
import repository.UserRepository;

public class Login {

		@Autowired
		private UserRepository userRepository;
		
		  public String login(@ModelAttribute("user") User user,
				  //	@ModelAttribute("roots") ArrayList<Category> roots,
				  	@RequestParam(value="username") String username, 
				  	@RequestParam(value="password") String password) {
			  if (username == null || password == null)
				  
				  return "login";
			  
			  User userTemp = userRepository.findByUsernameAndPassword(username, password);
			  if (userTemp == null) {
				  return "login";
			  } else {
				  user.setId(userTemp.getId());
				  user.setFirst_name(userTemp.getFirst_name());
				  user.setLast_name(userTemp.getLast_name());
				  System.out.print("login uspesan");
			  }
	
			  
			  return "login";
		  }
	
}
