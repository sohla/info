package controller;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import models.Flight;
import repository.FlightRepository;


public class FlightController {

	private static final String VIEWS_FLIGHT_CREATE_OR_UPDATE_FORM = "flights/createOrUpdateFlightForm";
	private final FlightRepository flights;

	public FlightController(FlightRepository flights) {
		this.flights = flights;
	}
	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}
	@GetMapping("/flights/new")
	public String initCreationForm(Map<String, Object> model) {
		
		Flight flight = new Flight(0, 0, null, null, null, null, 0);	
		model.put("flight", flight);
		return VIEWS_FLIGHT_CREATE_OR_UPDATE_FORM;
		
	}
	
	@PostMapping("/flights/new")
	public String processCreationForm(@Valid Flight flight, BindingResult result) {
		if (result.hasErrors()) {
			return VIEWS_FLIGHT_CREATE_OR_UPDATE_FORM;
		}
		else {
			this.flights.save(flight);
			return "redirect:/owners/" + flight.getId();
		}
	}
	
	@GetMapping("/flights/find")
	public String initFindForm(Map<String, Object> model) {
		// dodato nul nul 0 0 i to sve
		model.put("flight", new Flight(0, 0, null, null, null, null, 0));
		return "flights/findflights";
	}
/*	
	@GetMapping("/flights")
	public String processFindForm(Flight flight,BindingResult result, Map<String, Object> model) {
		
		if (flight.getId() == null) {
			flight.setId(""); // empty string signifies broadest possible search
		}

		// find owners by last name
		Flight results = this.flights.findById(flight.getId());
		if (results.isEmpty()) {
			// no owners found
			result.rejectValue("lastName", "notFound", "not found");
			return "owners/findOwners";
		}
		else if (results.size() == 1) {
			// 1 owner found
			flight = results.iterator().next();
			return "redirect:/owners/" + flight.getId();
		}
		else {
			// multiple owners found
			model.put("selections", results);
			return "flights/flightsList";
		}
		
	}
*/	
	@GetMapping("/flights/{id}/edit")
	public String initUpdateOwnerForm(@PathVariable("id") int id, Model model) {
		Optional<Flight> flight = this.flights.findById(id);
		model.addAttribute(flight);
		return VIEWS_FLIGHT_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/owners/{ownerId}/edit")
	public String processUpdateOwnerForm(@Valid Flight flight, BindingResult result,
			@PathVariable("id") int id) {
		if (result.hasErrors()) {
			return VIEWS_FLIGHT_CREATE_OR_UPDATE_FORM;
		}
		else {
			flight.setId(id);
			this.flights.save(flight);
			return "redirect:/flights/{id}";
		}
	}
	@GetMapping("/flights/{id}")
	public ModelAndView showOwner(@PathVariable("id") int id) {
		ModelAndView mav = new ModelAndView("flights/flightDetails");
		Optional<Flight> flight = this.flights.findById(id);
//  za karte kupljene / rezervisane		
//		for (Pet pet : flight.getPets()) {
//			pet.setVisitsInternal(visits.findByPetId(pet.getId()));
//		}
	
		mav.addObject(flight);
		return mav;
	}
	
}
