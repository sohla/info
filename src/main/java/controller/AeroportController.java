package controller;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import models.Aeroport;
import repository.AeroportRepository;


public class AeroportController {

	private static final String VIEWS_AEROPORT_CREATE_OR_UPDATE_FORM = "Aeroport/createOrUpdateAeroportForm";
	private final AeroportRepository aeroports;
	
//login


	public AeroportController(AeroportRepository aeroports) {
		this.aeroports = aeroports;
	}
	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}
	@GetMapping("/aeroports/new")
	public String initCreationForm(Map<String, Object> model) {
		
		Aeroport aeroport = new Aeroport();	
		model.put("aeroport", aeroport);
		return VIEWS_AEROPORT_CREATE_OR_UPDATE_FORM;
		
	}
	
	@PostMapping("/aeroports/new")
	public String processCreationForm(@Valid Aeroport aeroport, BindingResult result) {
		if (result.hasErrors()) {
			return VIEWS_AEROPORT_CREATE_OR_UPDATE_FORM;
		}
		else {
			this.aeroports.save(aeroport);
			return "redirect:/owners/" + aeroport.getId();
		}
	}
	
	@GetMapping("/aeroports/find")
	public String initFindForm(Map<String, Object> model) {
		model.put("aeroport", new Aeroport());
		return "aeroports/findaeroports";
	}
	
	@GetMapping("/aeroports")
	public String processFindForm(Aeroport aeroport,BindingResult result, Map<String, Object> model) {
		
		if (aeroport.getName() == null) {
			aeroport.setName(""); // empty string signifies broadest possible search
		}

		// find owners by last name
		Collection<Aeroport> results = this.aeroports.findByName(aeroport.getName());
		if (results.isEmpty()) {
			// no owners found
			result.rejectValue("lastName", "notFound", "not found");
			return "owners/findOwners";
		}
		else if (results.size() == 1) {
			// 1 owner found
			aeroport = results.iterator().next();
			return "redirect:/owners/" + aeroport.getId();
		}
		else {
			// multiple owners found
			model.put("selections", results);
			return "owners/ownersList";
		}
		
	}
	
	@GetMapping("/aeroports/{id}/edit")
	public String initUpdateOwnerForm(@PathVariable("id") int id, Model model) {
		Optional<Aeroport> aeroport = this.aeroports.findById(id);
		model.addAttribute(aeroport);
		return VIEWS_AEROPORT_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/aeroports/{ownerId}/edit")
	public String processUpdateOwnerForm(@Valid Aeroport aeroport, BindingResult result,
			@PathVariable("ownerId") int id) {
		if (result.hasErrors()) {
			return VIEWS_AEROPORT_CREATE_OR_UPDATE_FORM;
		}
		else {
			aeroport.setId(id);
			this.aeroports.save(aeroport);
			return "redirect:/aeroports/{id}";
		}
	}
	@GetMapping("/aeroports/{id}")
	public ModelAndView showOwner(@PathVariable("id") int id) {
		ModelAndView mav = new ModelAndView("aeroports/aeroportDetails");
		Optional<Aeroport> aeroport = this.aeroports.findById(id);
//  za karte kupljene / rezervisane		
//		for (Pet pet : aeroport.getPets()) {
//			pet.setVisitsInternal(visits.findByPetId(pet.getId()));
//		}
	
		mav.addObject(aeroport);
		return mav;
	}
	
}
