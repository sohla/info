package repository;


import models.Flight;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface FlightRepository extends CrudRepository<Flight, Integer> {

	/*
	@Query("SELECT * FROM flights")
	@Transactional(readOnly = true)
	Collection<Flight> findAll();
	
	@Query("SELECT flights FROM flights WHERE flight_id =:id")
	@Transactional(readOnly = true)
	*/
	Flight findById(@Param("id") Long id);

	void remove(Integer id);	
	
	//void save(Flight flight);

}
