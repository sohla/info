package repository;

import models.Ticket;
import models.User;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface TicketRepository extends Repository<Ticket, Integer> {

	/*
	@Query("SELECT * FROM tickets")
	@Transactional(readOnly = true)
	Collection<User> findAll();
	
	@Query("SELECT ticketa FROM tickets WHERE ticket_id =:id")
	@Transactional(readOnly = true)
	*/
	Ticket findById(@Param("id") Integer id);	
	
	Ticket save(Ticket ticket);

	List<Ticket> findAll();

	void remove(Integer id);

}
