package repository;


import models.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface UserRepository extends CrudRepository<User, Integer> {
	
	/*
	@Query("SELECT * FROM users")
	@Transactional(readOnly = true)
	Collection<User> findAll();
	
	@Query("SELECT users FROM users WHERE user_id =:id")
	@Transactional(readOnly = true)
	*/
	Optional<User> findById(@Param("id") Integer id);	
	
	//void save(User user);

	User findByUsernameAndPassword(String username, String password);

	Collection<User> findByLastName(String last_name);

	User findByUsername(String username);

	void remove(Integer id);

}
