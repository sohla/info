package repository;

import models.Aeroport;
import models.User;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AeroportRepository extends CrudRepository<Aeroport, Integer> {
/*	
	@Query("SELECT * FROM users")
	@Transactional(readOnly = true)
	Collection<User> findAll();
	
	@Query("SELECT user FROM users WHERE user_id =:id")
	@Transactional(readOnly = true)
*/
	Aeroport findById(@Param("id") Long id);	
	
	//void save(@Valid Aeroport aeroport);

	List<Aeroport> findByName(String name);

	void remove(Integer id);

}
