package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
public class Ticket {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	//verovatno je aerodrom a ne flight, proveri.
	
	@Column(name = "start")
	@NotEmpty
	String start;
	
	@Column(name = "finish")
	@NotEmpty
	String finish;

	@Column(name = "seat_a")
	@NotEmpty	
	String seat_a;
	
	@Column(name = "seat_b")
	@NotEmpty
	String seat_b;
	
	@Column(name = "date_reservation")
	@NotEmpty
	String date_reservation;

	@Column(name = "date_sold")
	@NotEmpty	
	String date_sold;
	
	@Column(name = "user")
	@NotEmpty	
	String user;
	
	@Column(name = "first_name")
	@NotEmpty
	String first_name;
	
	@Column(name = "last_name")
	@NotEmpty	
	String last_name;
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public @NotEmpty String getStart() {
		return start;
	}


	public void setStart(String start) {
		this.start = start;
	}


	public @NotEmpty  String getFinish() {
		return finish;
	}


	public void setFinish(String finish) {
		this.finish = finish;
	}


	public String getSeat_a() {
		return seat_a;
	}


	public void setSeat_a(String seat_a) {
		this.seat_a = seat_a;
	}


	public String getSeat_b() {
		return seat_b;
	}


	public void setSeat_b(String seat_b) {
		this.seat_b = seat_b;
	}


	public String getDate_reservation() {
		return date_reservation;
	}


	public void setDate_reservation(String date_reservation) {
		this.date_reservation = date_reservation;
	}


	public String getDate_sold() {
		return date_sold;
	}


	public void setDate_sold(String date_sold) {
		this.date_sold = date_sold;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public Ticket(int id, String start, String  finish, String seat_a, String seat_b, String date_reservation,
			String date_sold, String user, String first_name, String last_name) {
		super();
		this.id = id;
		this.start = start;
		this.finish = finish;
		this.seat_a = seat_a;
		this.seat_b = seat_b;
		this.date_reservation = date_reservation;
		this.date_sold = date_sold;
		this.user = user;
		this.first_name = first_name;
		this.first_name = last_name;
	}


	@Override
	public String toString() {
		return "Ticket [id=" + id + ", start=" + start + ", finish=" + finish + ", seat_a=" + seat_a + ", seat_b="
				+ seat_b + ", date_reservation=" + date_reservation + ", date_sold=" + date_sold + ", user=" + user
				+ ", first_name=" + first_name + ", last_name=" + last_name + "]";
	}



	
	
}
