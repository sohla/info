package models;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
public class Flight {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	int id;

	@Column(name = "number")
	@NotEmpty
	int number;

	@Column(name = "date_a")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@ManyToMany
	Date date_a;
	
	@Column(name = "date_b")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	Date date_b;
	
	@Column(name = "aeroport_a")
	@NotEmpty
	String aeroport_a;
	
	@Column(name = "aeroport_b")
	@NotEmpty

	String aeroport_b;
	
	@Column(name = "seat_number")
	@NotEmpty
	int seat_number;
	
	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getNumber() {
		return number;
	}



	public void setNumber(int number) {
		this.number = number;
	}



	public Date getDate_a() {
		return date_a;
	}



	public void setDate_a(Date date_a) {
		this.date_a = date_a;
	}



	public Date getDate_b() {
		return date_b;
	}



	public void setDate_b(Date date_b) {
		this.date_b = date_b;
	}



	public @NotEmpty String getAeroport_a() {
		return aeroport_a;
	}



	public void setAeroport_a(String aeroport_a) {
		this.aeroport_a = aeroport_a;
	}



	public @NotEmpty String getAeroport_b() {
		return aeroport_b;
	}



	public void setAeroport_b(String aeroport_b) {
		this.aeroport_b = aeroport_b;
	}



	public int getSeat_number() {
		return seat_number;
	}



	public void setSeat_number(int seat_number) {
		this.seat_number = seat_number;
	}



	public Flight(int id, int number, Date date_a, Date date_b, String aeroport_a, String aeroport_b,
			int seat_number) {
		super();
		this.id = id;
		this.number = number;
		this.date_a = date_a;
		this.date_b = date_b;
		this.aeroport_a = aeroport_a;
		this.aeroport_b = aeroport_b;
		this.seat_number = seat_number;
	}



	@Override
	public String toString() {
		return "Flight [id=" + id + ", number=" + number + ", date_a=" + date_a + ", date_b=" + date_b + ", aeroport_a="
				+ aeroport_a + ", aeroport_b=" + aeroport_b + ", seat_number=" + seat_number + ", getId()=" + getId()
				+ ", getNumber()=" + getNumber() + ", getDate_a()=" + getDate_a() + ", getDate_b()=" + getDate_b()
				+ ", getAeroport_a()=" + getAeroport_a() + ", getAeroport_b()=" + getAeroport_b()
				+ ", getSeat_number()=" + getSeat_number() + "]";
	}


	
}