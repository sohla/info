package models;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	int id;
	
	@Column(name="username")
	@NotEmpty
	String username;
	
	@Column(name="password")
	@NotEmpty
	String password;
	
	@Column(name="first_name")
	@NotEmpty
	String first_name;
	
	@Column(name="last_name")
	@NotEmpty
	String last_name;	
	
	@Column(name="date")
	//@NotEmpty(message = "Please enter date")
	Date date;

	@Column(name="role")
	@NotEmpty
	String role;
	
	
	boolean blocked;	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String string) {
		this.role = string;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	
	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public User(int id, String username, String password, Date date, String role, String first_name, String last_name, boolean blocked) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.date = date;
		this.role = role;
		this.first_name = first_name;
		this.last_name = last_name;
		this.blocked = blocked;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", date=" + date + ", role=" + role + ", blocked=" + blocked
				+ ", getId()=" + getId() + ", getUsername()=" + getUsername() + ", getPassword()=" + getPassword()
				+ ", getDate()=" + getDate() + ", getRole()=" + getRole() + ", isBlocked()=" + isBlocked()
				+ ", getFirst_name()=" + getFirst_name() + ", getLast_name()=" + getLast_name() + "]";
	}







	
	

}