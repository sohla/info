package models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


public class RegisterForm {

	@NotEmpty
	@Size(min=5, max=30)
	private String username = "";
	
	@NotEmpty
	@Size(min=5, max=30)
	private String password = "";
	
	@NotEmpty
	@Size(min=5, max=30)
	private String passwordRepeat = "";
	
	@NotEmpty
	@Size(min=5, max=30)
	private String Date = "";
	
	@NotEmpty
	@Size(min=5, max=30)
	private String firstName = "";
	
	@NotEmpty
	@Size(min=5, max=30)
	private String lastName = "";
	
	@NotEmpty
	@Size(min=5, max=30)
	private String role = "BUYER";

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}


	
	
}
