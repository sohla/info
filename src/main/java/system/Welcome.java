package system;

import javax.validation.Valid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import models.LoginForm;



@SpringBootApplication(proxyBeanMethods = false)
public class Welcome extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Welcome.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Welcome.class, args);
	}  
}

/**
@Controller
class WelcomeController implements WebMvcConfigurer {
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/results").setViewName("results");
	}

	
	@GetMapping("/")
	public String showForm(LoginForm loginForm) {
		return "login";
	}

	@PostMapping("/")
	public String checkPersonInfo(@Valid LoginForm loginForm, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return "login";
		}

		return "redirect:/login";
	}

}
**/